# Copyright 2017-2018 Rasmus thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ suffix=tar.xz release=${PV} user=flatpak ]

export_exlib_phases src_prepare src_test src_install

SUMMARY="Tool to build flatpaks from source"
HOMEPAGE="https://flatpak.org/"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    doc
    ( providers: elfutils libelf ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.18.2]
        virtual/pkg-config[>=0.24]
        doc? (
            app-text/docbook-xml-dtd:4.3
            app-text/docbook-xsl-stylesheets
            app-text/xmlto
            dev-libs/libxslt
        )
    build+run:
        core/json-glib
        dev-libs/glib:2[>=2.44]
        dev-libs/libxml2:2.0[>=2.4]
        dev-libs/libyaml
        dev-util/debugedit[>=5.0]
        net-misc/curl
        gnome-desktop/libsoup:2.4
        sys-apps/flatpak[>=0.99.1]
        sys-devel/libostree:1[>=2017.14]
        providers:elfutils? ( dev-util/elfutils )
        providers:libelf? ( dev-libs/libelf[>=0.8.12] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    # Optimizations for libostree build again fuse:3
    --with-fuse=3
    --with-system-debugedit
    --with-yaml
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'doc documentation'
    'doc docbook-docs'
)

# test-libgnlx-fdio fails because of sydbox
RESTRICT="test"

flatpak-builder_src_prepare() {
    default

    # Don't use /var/tmp as test dir, but ${TEMP}
    edo sed -e "s:/var/tmp/test-flatpak-XXXXXX:"${TEMP%/}"/test-flatpak-XXXXXX:" -i \
        tests/libtest.sh
}

flatpak-builder_src_test() {
    esandbox allow_net "unix:${TEMP%/}/test-flatpak-*/gpghome/S.gpg-agent"
    esandbox allow_net --connect "unix:${TEMP%/}/test-flatpak-*/gpghome/S.gpg-agent"
    esandbox allow_net "unix:${TEMP%/}/test-flatpak-*/gpghome/S.gpg-agent.extra"
    esandbox allow_net --connect "unix:${TEMP%/}/test-flatpak-*/gpghome/S.gpg-agent.extra"

    default

    esandbox disallow_net "unix:${TEMP%/}/test-flatpak-*/gpghome/S.gpg-agent.extra"
    esandbox disallow_net --connect "unix:${TEMP%/}/test-flatpak-*/gpghome/S.gpg-agent.extra"
    esandbox disallow_net "unix:${TEMP%/}/test-flatpak-*/gpghome/S.gpg-agent"
    esandbox disallow_net --connect "unix:${TEMP%/}/test-flatpak-*/gpghome/S.gpg-agent"
}

flatpak-builder_src_install() {
    default

    # Remove empty dir
    edo rmdir "${IMAGE}"/usr/$(exhost --target)/libexec
}

