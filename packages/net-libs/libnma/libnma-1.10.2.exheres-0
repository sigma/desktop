# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2019 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings meson
require vala [ vala_dep=true with_opt=true ]

SUMMARY="NetworkManager GUI library"
HOMEPAGE="http://www.gnome.org/projects/NetworkManager"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    (
        providers:
            gtk4 [[ description = [ Build libnma-gtk4 ] ]]
    )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.18]
        virtual/pkg-config[>=0.20]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.6] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.0] )
    build+run:
        app-text/iso-codes
        dev-libs/glib:2[>=2.38]
        gnome-desktop/gcr:0[>=3.14]
        net-apps/NetworkManager[>=1.8]
        net-misc/mobile-broadband-provider-info
        x11-libs/cairo
        x11-libs/gtk+:3[>=3.12]
        providers:gtk4? ( x11-libs/gtk:4.0[>=4.0] )
        !gnome-desktop/network-manager-applet[<1.16] [[
            description = [ libnma has been split out of nm-applet ]
            resolution = uninstall-blocked-before
        ]]
    run:
        sys-apps/dbus[>=1.2.6]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dgcr=true
    -Dgcr_gtk4=false
    -Dmore_asserts="0"
    -Diso_codes=true
    -Dmobile_broadband_provider_info=true
    -Dld_gc=true
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gtk-doc gtk_doc'
    'gobject-introspection introspection'
    'providers:gtk4 libnma_gtk4'
    vapi
)
