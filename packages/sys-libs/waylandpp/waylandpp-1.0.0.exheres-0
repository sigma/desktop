# Copyright 2017-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=NilsBrause ] cmake

SUMMARY="Wayland C++ bindings"

LICENCES="
    BSD-2 [[ note = [ includes and src ] ]]
    GPL-3 [[ note = [ scanner ] ]]
    MIT [[ note = [ protocols ] ]]
"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        dev-libs/libglvnd[?X] [[ note = [ EGL/egl.h ] ]]
        virtual/pkg-config
        doc? ( app-doc/doxygen[dot] )
    build+run:
        dev-libs/pugixml[>=1.9-r1]
        sys-libs/wayland[>=1.17.0]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_INSTALL_DOCDIR=/usr/share/doc/${PNVR}
    -DBUILD_EXAMPLES:BOOL=FALSE
    -DBUILD_LIBRARIES:BOOL=TRUE
    -DBUILD_SCANNER:BOOL=TRUE
    -DBUILD_SERVER:BOOL=TRUE
    -DINSTALL_UNSTABLE_PROTOCOLS:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_BUILDS=(
    'doc DOCUMENTATION'
)

