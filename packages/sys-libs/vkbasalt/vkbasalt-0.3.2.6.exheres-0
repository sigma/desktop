# Copyright 2020-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

RESHADE_SHADERS_REV="a2314cbf9a6c96de4d434b2f9ab8fa9265b8e575"

require github [ user=DadSchoorse project=vkBasalt release=v${PV} suffix=tar.gz ] \
    meson

SUMMARY="Vulkan post processing layer to enhance the visual graphics"
DESCRIPTION="
Build in effects are:
* Contrast Adaptive Sharpening
* Fast Approximate Anti-Aliasing
* Enhanced Subpixel Morphological Anti-Aliasing
* Deband/Dithering
* 3D color LookUp Table
It is also possible to use Reshade Fx shaders.
"

DOWNLOADS+="
    https://github.com/crosire/reshade-shaders/archive/${RESHADE_SHADERS_REV}.tar.gz -> reshade-shaders-${RESHADE_SHADERS_REV}.tar.gz
"

LICENCES="as-is"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-lang/glslang
        dev-lang/spirv-tools
        sys-libs/spirv-headers
        sys-libs/vulkan-headers
        sys-libs/vulkan-validation-layers
        virtual/pkg-config
    build+run:
        x11-libs/libX11
"

MESON_SOURCE=${WORKBASE}/vkBasalt-${PV}

MESON_SRC_CONFIGURE_PARAMS=(
    -Dappend_libdir_vkbasalt=true
    -Dwith_json=true
    -Dwith_so=true
)

src_prepare() {
    meson_src_prepare

    # enable ReShade support
    edo sed \
        -e 's:/path/to/reshade-shaders/Shaders:/usr/share/vkBasalt/reshade-shaders/Shaders:g' \
        -e 's:/path/to/reshade-shaders/Textures:/usr/share/vkBasalt/reshade-shaders/Textures:g' \
        -i config/vkBasalt.conf
}

src_install() {
    meson_src_install

    insinto /etc/vkBasalt
    doins config/vkBasalt.conf

    insinto /usr/share/vkBasalt/reshade-shaders
    doins -r "${WORKBASE}"/reshade-shaders-${RESHADE_SHADERS_REV}/*
}

